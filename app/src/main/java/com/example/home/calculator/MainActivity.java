package com.example.home.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.editText)
    TextView editText;

    String firstValue;
    String secondValue;
    String operation;
    boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5,
            R.id.button6, R.id.button7, R.id.button8, R.id.button9, R.id.button0, R.id.button10})
    public void makeText(Button button) {
        editText.setText(String.format("%s%s", editText.getText(), button.getText()));
        if (!flag) {
            if (firstValue == null) {
                firstValue = (String) button.getText();
            } else {
                firstValue += (String) button.getText();
            }

        } else {
            if (secondValue == null) {
                secondValue = (String) button.getText();
            } else {
                secondValue += (String) button.getText();
            }
        }
    }

    @OnClick({R.id.buttonadd, R.id.buttonsub, R.id.buttonmul, R.id.buttondiv,})
    public void operation(Button button) {
        operation = (String) button.getText();
        editText.setText(String.format("%s%s", editText.getText(), button.getText()));
        flag = true;
    }

    @OnClick(R.id.buttonC)
    public void clear() {
        flag = false;
        firstValue = null;
        secondValue = null;
        editText.setText(null);
    }

    @OnClick(R.id.buttoneql)
    public void equal() {
        flag = false;
        float first = Float.parseFloat(firstValue);
        float second = Float.parseFloat(secondValue);
        float result = 0;

        if (operation.equals("+")) {
            result = first + second;
        }
        if (operation.equals("-")) {
            result = first - second;
        }
        if (operation.equals("*")) {
            result = first * second;
        }
        if (operation.equals("/")) {
            result = first / second;
        }
        editText.setText(String.format("%s", result));
        firstValue = Float.toString(result);
        secondValue = null;
    }
}
